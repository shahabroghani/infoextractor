package com.infoextractor.infoextractor.view;

import com.infoextractor.infoextractor.services.InfoExtractorFacade;
import com.infoextractor.infoextractor.services.TextInfo;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Route("")
public class MainView extends VerticalLayout {
    private InfoExtractorFacade infoExtractor;

    @Autowired
    public MainView(InfoExtractorFacade infoExtractor){
        this.infoExtractor = infoExtractor;
        Label nameLabel = new Label("Enter text name:");
        TextField name = new TextField();
        Label label = new Label("Enter your text:");
        TextArea input = new TextArea();
        Grid<TextInfo> grid = new Grid<>();
        grid.setHeightByRows(true);
        grid.addColumn(TextInfo::getName).setHeader("Name").setSortable(true);
        grid.addColumn(TextInfo::getCharCount).setHeader("Char Count").setSortable(true);
        grid.addColumn(TextInfo::getWordCount).setHeader("Word Count").setSortable(true);
        grid.addColumn(TextInfo::getSentenceCount).setHeader("Sentence Count").setSortable(true);
        grid.addColumn(TextInfo::getMostUseChar).setHeader("Most Used Char").setSortable(true);
        grid.addColumn(TextInfo::getMostUseWord).setHeader("Most Used Word").setSortable(true);

        Button button = new Button("Extract",
                event-> eventHandler(input,name,grid));

        add(
                new H1("InfoExtractor"),
                nameLabel,
                name,
                label,
                input,
                button,
                grid
        );
        updateGrid(grid);
    }

    private void eventHandler(TextArea input,TextField name,Grid<TextInfo> grid){
        infoExtractor.extract(input.getValue(),name.getValue());
        updateGrid(grid);
    }

    private void updateGrid(Grid<TextInfo> grid){
        Iterable<TextInfo> textInfos = infoExtractor.findAll();
        if (textInfos.iterator().hasNext()){
            List<TextInfo> textInfosList = new ArrayList<>();
            textInfos.forEach(textInfosList::add);
            if (textInfos.iterator().hasNext()) {
                grid.setDataProvider(DataProvider.ofCollection(textInfosList));
            }
        }
    }


}
