package com.infoextractor.infoextractor.repo;

import com.infoextractor.infoextractor.services.TextInfo;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TextInfoRepository extends CrudRepository<TextInfo,Long> {
}
