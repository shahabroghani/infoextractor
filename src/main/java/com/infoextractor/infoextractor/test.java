package com.infoextractor.infoextractor;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class test {
    public static void main(String[] args) {
        System.out.println(count("mohammad ebrahim khah"));
    }
    public static Character count(String txt) {
        Character c = ' ';
        Map<Character,Integer> characterIntegerMap = new HashMap<>();
        for (int i=0;i<txt.length();i++){
            characterIntegerMap.put(txt.charAt(i), characterIntegerMap.getOrDefault(txt.charAt(i), 0)+1);
        }
        Collection<Integer> values = characterIntegerMap.values();
        Integer max = Collections.max(values);
        for (Map.Entry entry:characterIntegerMap.entrySet()){
            if (max==entry.getValue()){
                c = (Character) entry.getKey();
            }
        }
        return c;
    }
}
