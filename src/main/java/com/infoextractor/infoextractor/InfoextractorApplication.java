package com.infoextractor.infoextractor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InfoextractorApplication {

    public static void main(String[] args) {
        SpringApplication.run(InfoextractorApplication.class, args);
    }

}
