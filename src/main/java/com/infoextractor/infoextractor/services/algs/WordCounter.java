package com.infoextractor.infoextractor.services.algs;

import com.infoextractor.infoextractor.services.ServiceInterface;
import org.springframework.stereotype.Service;
import java.util.StringTokenizer;
@Service
public class WordCounter implements ServiceInterface<Integer> {

    @Override
    public Integer apply(String s) {
        StringTokenizer stringTokenizer = new StringTokenizer(s," ,.\n\"'=+-_@#$%^&*(){}][\\|!?/><~");
        return stringTokenizer.countTokens();
    }
}
