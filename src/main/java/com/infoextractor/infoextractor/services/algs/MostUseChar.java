package com.infoextractor.infoextractor.services.algs;

import com.infoextractor.infoextractor.services.ServiceInterface;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Service
public class MostUseChar implements ServiceInterface<Character> {

    @Override
    public Character apply(String s) {
        Character c = ' ';
        char thisOne = ' ';
        Map<Character,Integer> characterIntegerMap = new HashMap<>();
        for (int i=0;i<s.length();i++){
            thisOne = s.charAt(i);
            if (thisOne!=' ')
                characterIntegerMap.put(thisOne, characterIntegerMap.getOrDefault(thisOne, 0)+1);
        }
        Collection<Integer> values = characterIntegerMap.values();
        Integer max = Collections.max(values);
        for (Map.Entry entry:characterIntegerMap.entrySet()){
            if (max==entry.getValue()){
                c = (Character) entry.getKey();
            }
        }
        return c;
    }
}
