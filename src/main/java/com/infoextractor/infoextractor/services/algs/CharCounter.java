package com.infoextractor.infoextractor.services.algs;
import com.infoextractor.infoextractor.services.ServiceInterface;
import org.springframework.stereotype.Service;

@Service
public class CharCounter implements ServiceInterface<Integer> {

    @Override
    public Integer apply(String s) {
        s = s.trim();
        return s.length();
    }
}
