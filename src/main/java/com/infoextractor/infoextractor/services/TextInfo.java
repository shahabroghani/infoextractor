package com.infoextractor.infoextractor.services;

import org.hibernate.annotations.ValueGenerationType;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;

@Entity
public class TextInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long Id;
    private Integer charCount;
    private Integer wordCount;
    private Integer sentenceCount;
    private Character mostUseChar;
    private String mostUseWord;
    private String name;

    public TextInfo() {
    }

    @Autowired
    public TextInfo(Integer charCount, Integer wordCount, Integer sentenceCount, Character mostUseChar, String mostUseWord) {
        this.charCount = charCount;
        this.wordCount = wordCount;
        this.sentenceCount = sentenceCount;
        this.mostUseChar = mostUseChar;
        this.mostUseWord = mostUseWord;
        this.name = name;
    }

    public Integer getCharCount() {
        return charCount;
    }

    public void setCharCount(Integer charCount) {
        this.charCount = charCount;
    }

    public Integer getWordCount() {
        return wordCount;
    }

    public void setWordCount(Integer wordCount) {
        this.wordCount = wordCount;
    }

    public Integer getSentenceCount() {
        return sentenceCount;
    }

    public void setSentenceCount(Integer sentenceCount) {
        this.sentenceCount = sentenceCount;
    }

    public Character getMostUseChar() {
        return mostUseChar;
    }

    public void setMostUseChar(Character mostUseChar) {
        this.mostUseChar = mostUseChar;
    }

    public String getMostUseWord() {
        return mostUseWord;
    }

    public void setMostUseWord(String mostUseWord) {
        this.mostUseWord = mostUseWord;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "TextInfo{" +
                "charCount=" + charCount +
                ", wordCount=" + wordCount +
                ", sentenceCount=" + sentenceCount +
                ", mostUseChar=" + mostUseChar +
                ", mostUseWord='" + mostUseWord + '\'' +
                '}';
    }
}
