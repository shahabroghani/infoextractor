package com.infoextractor.infoextractor.services;

import com.infoextractor.infoextractor.repo.TextInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InfoExtractorFacade {
//
//    private Collection<TextInfo> textInfos = new ArrayList<>();
    private CompositeInfoExtractor compositeInfoExtractor;
    private TextInfoRepository textInfoRepository;

    @Autowired
    public InfoExtractorFacade(CompositeInfoExtractor compositeInfoExtractor,TextInfoRepository textInfoRepository) {
        this.compositeInfoExtractor = compositeInfoExtractor;
        this.textInfoRepository = textInfoRepository;
    }

    public void extract(String s,String name){
        TextInfo textInfo;
        textInfo = compositeInfoExtractor.apply(s);
        textInfo.setName(name);
        textInfoRepository.save(textInfo);
    }

    public Iterable findAll(){
        return textInfoRepository.findAll();
    }

}
