package com.infoextractor.infoextractor.services;

import com.infoextractor.infoextractor.services.algs.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class CompositeInfoExtractor implements ServiceInterface<TextInfo> {

    private CharCounter charCounter;
    private WordCounter wordCounter;
    private SentenceCounter sentenceCounter;
    private MostUseChar mostUseChar;
    private MostUseWord mostUseWord;

    @Autowired
    public CompositeInfoExtractor(CharCounter charCounter, WordCounter wordCounter, SentenceCounter sentenceCounter, MostUseChar mostUseChar, MostUseWord mostUseWord) {
        this.charCounter = charCounter;
        this.wordCounter = wordCounter;
        this.sentenceCounter = sentenceCounter;
        this.mostUseChar = mostUseChar;
        this.mostUseWord = mostUseWord;
    }

    @Override
    public TextInfo apply(String s) {
        TextInfo textInfo = new TextInfo();
        textInfo.setCharCount(charCounter.apply(s));
        textInfo.setWordCount(wordCounter.apply(s));
        textInfo.setSentenceCount(sentenceCounter.apply(s));
        textInfo.setMostUseChar(mostUseChar.apply(s));
        textInfo.setMostUseWord(mostUseWord.apply(s));
        return textInfo;
    }
}
