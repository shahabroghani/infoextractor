package com.infoextractor.infoextractor.services;

import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
public interface ServiceInterface<T> extends Function<String,T> {

    @Override
    T apply(String s);
}
